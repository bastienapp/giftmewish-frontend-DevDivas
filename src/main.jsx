import React from "react";
import ReactDOM from "react-dom/client";
import { createBrowserRouter, RouterProvider } from "react-router-dom";
import "./index.css";
import Home from "./pages/Home";
import Login from "./pages/Login";
import Profile from "./pages/Profile";
import Register from "./pages/Register";
import CreateWishlistForm from "./pages/CreateWishlistForm";
import GiftForm from "./components/giftForm";

const router = createBrowserRouter([
  {
    path: "/",
    element: <Home />,
  },
  {
    path: "/login",
    element: <Login />,
  },
  {
    path: "/register",
    element: <Register/>,
  },
  {
    path: "/profile",
    element: <Profile />,
  },
  {
    path: "/create-wishlist",
    element: <CreateWishlistForm />,
  },
  {
    path: "/create-gift",
    element: <GiftForm />,
  },
]);

ReactDOM.createRoot(document.getElementById("root")).render(
  <React.StrictMode>
    <RouterProvider router= {router}/>
  </React.StrictMode>
);
