import "../styles/WishlistItem.css";

const WishlistItem = (props) => {

    const { nom, theme} = props;
  

  return (
    <>
      <div className="WishlistItem">
              <h2>{nom}</h2>
              <h3>{theme}</h3>
      </div>
    </>
  );
};

export default WishlistItem;
