import "../styles/Footer.css";

const Footer = () => {
  return (
    <footer className="Footer">
      <p> DevDivas &copy; 2024 </p>
    </footer>
  );
};

export default Footer;
