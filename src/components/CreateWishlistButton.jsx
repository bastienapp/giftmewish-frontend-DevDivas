import "../styles/CreateWishlistButton.css";
import { Link } from "react-router-dom";

const CreateWishlistButton = () => {
  return (
    <div className="CreateWishlist">
      <h2 className="CreateWishlist-text">Créer une nouvelle liste</h2>
      <Link to="/create-wishlist">
        <div className="CreateWishlist-container">
          <div className="create-wishlist-button">
            <span>+</span>
          </div>
        </div>
      </Link>
    </div>
  );
};

export default CreateWishlistButton;
