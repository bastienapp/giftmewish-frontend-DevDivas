import { useState, useEffect } from "react";
import axios from "axios";
import "../styles/MyWishlists.css";
import WishlistItem from "./WishlistItem";

const SharedWishlists = () => {
  const [data, setData] = useState(null);
  const [error, setError] = useState(null);

  //TODO afficher toutes les wishlists partagées à un utilisateur (endpoint utilisateurs//:idUtilisateur/wishlists/partage)
  
  useEffect(() => {
    axios
      .get("http://localhost:5000/utilisateurs/wishlists/partage")
      .then((response) => {
        setData(response.data);
      })
      .catch((error) => {
        if (typeof error.response === "undefined") {
          setError("Une erreur est survenue.");
        } else {
          setError(error.message);
        }
      });
  }, []);

  return (
    <>
      <div className="MyWishlists-tab">
        <h2 className="MyWishlists-tab--title">
          Wishlists partagées avec moi{" "}
        </h2>

        <div>
          {error === null && data === null && "Chargement..."}
          {error !== null && error}
          {error === null && data !== null && (
            <ul className="MyWishlists-tab--list">
              {data.map((eachWishlist) => (
                <li key={eachWishlist.id_wishlist}>
                  <WishlistItem
                    nom={eachWishlist.nom}
                    theme={eachWishlist.theme}
                  />
                </li>
              ))}
            </ul>
          )}
        </div>
      </div>
    </>
  );
};

export default SharedWishlists;