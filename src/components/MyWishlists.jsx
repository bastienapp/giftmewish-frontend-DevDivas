import { useState, useEffect } from "react";
import axios from "axios";
import "../styles/MyWishlists.css";
import CreateWishlistButton from "../components/CreateWishlistButton";
import WishlistItem from "./WishlistItem";

const MyWishlists = () => {

  const [data, setData] = useState(null);
  const [error, setError] = useState(null);

  useEffect(() => {
    axios
      .get("http://localhost:5000/wishlists")
      .then((response) => {
        setData(response.data);
      })
      .catch((error) => {
        if (typeof error.response === "undefined") {
          setError("Une erreur est survenue.");
        } else {
          setError(error.message);
        }
      });
  }, []);

  //TODO afficher seulement les wishlists de l'utilisateur (endpoint : wishlists//utilisateurs/:idUtilisateur)
  //TODO ajouter <Link> pour afficher la wishlist au clic (endpoint : à faire)
  //TODO ajouter raccourci sur chaque Wishlist pour la partager 

  return (
    <>
      <div className="MyWishlists-tab">
        <h2 className="MyWishlists-tab--title">Mes wishlists</h2>
        <CreateWishlistButton />

        <div>
          {error === null && data === null && "Chargement..."}
          {error !== null && error}
          {error === null && data !== null && (
            <ul className="MyWishlists-tab--list">
              {data.map((eachWishlist) => (
                <li key={eachWishlist.id_wishlist}>
                  <WishlistItem
                    nom={eachWishlist.nom}
                    theme={eachWishlist.theme}
                  />
                </li>
              ))}
            </ul>
          )}
        </div>
      </div>
    </>
  );
};

export default MyWishlists;