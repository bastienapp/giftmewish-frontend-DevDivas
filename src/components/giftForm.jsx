import  { useState } from 'react';
import PropTypes from 'prop-types';
import "../styles/giftForm.css";


const GiftForm = ({ onAddGift }) => {
  // État local pour stocker les données du formulaire
  const [gift, setGift] = useState({
    nom: '',
    description: '',
    lien: '',
    niveau_envie: '',
    prix_approximatif: '',
  });

  // Fonction de gestion pour mettre à jour l'état en fonction de la saisie de l'utilisateur
  const handleChange = (e) => {
    const { name, value } = e.target;
    setGift((PrevGift) => ({
      ...PrevGift,
      [name]: value,
    }));
  };

  // Fonction de gestion pour soumettre le formulaire
  const handleSubmit = (e) => {
    e.preventDefault();
    // Vérifier si toutes les données requises sont remplies
    if (gift.nom && gift.description && gift.lien && gift.niveau_envie && gift.prix_approximatif) {
      // Appeler la fonction fournie pour ajouter le cadeau
      onAddGift(gift);
      // Réinitialiser le formulaire après soumission
      setGift({
        nom: '',
        description: '',
        lien: '',
        niveau_envie: '',
        prix_approximatif: '',
      });
    } else {
      alert('Veuillez remplir tous les champs du formulaire.');
    }
  };

  return (
    <form className='form--addgift' onSubmit={handleSubmit}>
      <label><h1>Ajoutez votre cadeau</h1></label>
      <label>
        Nom du cadeau:
        <input  className="title--gift"
          type="text"
          name="nom"
          value={gift.nom}
          onChange={handleChange}
          required
        />
      </label>
      <br />
      <label >
        Description:
        <textarea className="textearea--gift" 
          name="description"
          value={gift.description}
          onChange={handleChange}
          required
        />
      </label>
      <br />
      <label >
        Lien:
        <input
        className="link--gift"
          type="text"
          name="lien"
          value={gift.lien}
          onChange={handleChange}
          required
        />
      </label>
      <br />
      <label >
        Niveau envie:
        <input className="niveau_envie--gift"
          type="text"
          name="niveau_envie"
          value={gift.niveau_envie}
          onChange={handleChange}
          required
        />
      </label>
      <br />
      <label >
        Prix approximatif:
        <input
        className="price--gift"
          type="number"
          name="prix_approximatif"
          value={gift.prix_approximatif}
          onChange={handleChange}
          required
        />
      </label>
      <br />
      <button className="button--gift" type="submit">Ajouter le cadeau</button>
    </form>
  );
};

// Validation des propriétés (PropTypes)
GiftForm.propTypes = {
    onAddGift: PropTypes.func.isRequired, // onAddGift doit être une fonction requise
  };

export default GiftForm;
