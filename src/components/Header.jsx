import "../styles/Header.css"; 
import logo from "../assets/logoWMG.png"
import Button from "./Button";

//TODO toggle du bouton une fois connecté (content = "nom profil/mon compte" link = /profile)

const Header = () => {
  return (
    <header className="Header">
      <div>
        <img className="logo" src={logo} alt="logo"></img>
      </div>
      <div>
        <Button content="Connexion" link="/Login"/>
      </div>
    </header>
  );
};

export default Header;
