import "../styles/Button.css";
import { Link } from "react-router-dom";

const Button = (props) => {
  const { content, link } = props;
  
  return (
    <Link to={link}>
      <button className="Button">{content}</button>
    </Link>
  );
};

export default Button;
