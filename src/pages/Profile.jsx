import { useState, useEffect } from "react";
import axios from "axios";
import Header from "../components/Header";
import Footer from "../components/Footer";
import "../styles/Profile.css";
import MyWishlists from "../components/MyWishlists";
import SharedWishlists from "../components/SharedWishlists";

const Profile = () => {

  const [activeTab, setActiveTab] = useState(1);
  const MYWISHLISTS_TAB = 1;
  const SHAREDWISHLISTS_TAB = 2;
  
  //TODO aller chercher le prenom nom de l'utilisateur (endpoint à faire : GET utilisateurs/:id_utilisateur) 
  //TODO afficher le nombre actuel de wishlists crées par cet utilisateur et partagées à cet utilisateur

  return (
    <>
      <Header />

      <div className="Profile-page">
        <div className="Profile-header">
          <div className="Profile-logo">DD</div>
          <h2>Dev DIVAS</h2>
        </div>

        <div className="tabs">
          <button
            className={`tab-button ${
              activeTab === MYWISHLISTS_TAB ? "active" : ""
            }`}
            onClick={() => setActiveTab(MYWISHLISTS_TAB)}
          >
            Mes wishlists (0)
          </button>
          <button
            className={`tab-button ${
              activeTab === SHAREDWISHLISTS_TAB ? "active" : ""
            }`}
            onClick={() => setActiveTab(SHAREDWISHLISTS_TAB)}
          >
            Wishlists partagées (0)
          </button>
        </div>

        <div>
          {activeTab === MYWISHLISTS_TAB && <MyWishlists />}
          {activeTab === SHAREDWISHLISTS_TAB && <SharedWishlists />}
        </div>
      </div>

      <Footer />
    </>
  );

}

export default Profile;
