import CreateWishlistButton from "../components/CreateWishlistButton";
import Footer from "../components/Footer";
import Header from "../components/Header";
import "../styles/Home.css";

const Home = () => {
  return (
    <>
      <Header />
      <div className="Home-content">
              <h1>Bienvenue sur WishMeGift</h1>
              <p>Créez, partagez et gérez toutes vos listes de souhait en un seul endroit ! </p>
              <h3>Prêt à commencer ?</h3>
        <CreateWishlistButton />
      </div>
      <Footer />
    </>
  );
};

export default Home;
