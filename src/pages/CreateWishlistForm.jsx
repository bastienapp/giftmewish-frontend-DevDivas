import { useState } from "react";
import "../styles/CreateWishlistForm.css";
import Footer from "../components/Footer";
import Header from "../components/Header";
import axios from "axios";

const CreateWishlistForm = () => {
  const [nom, setNom] = useState("");
  const [theme, setTheme] = useState("mariage");
  const [description, setDescription] = useState("");
  const [dateEvenement, setDateEvenement] = useState("");

      function handleSubmit(event) {
        event.preventDefault();
        axios
          .post("http://localhost:5000/wishlists", {
            nom: nom,
            theme: theme,
            description: description,
            date_evenement: dateEvenement,
           })
          
          .then((response) => console.log(response))
          .catch((error) => console.error(error.message));
      }
    

    

  return (
    <>
      <Header />
      <div className="CreateWishlistFormDiv">
        <h1 className="WishlistFormTitle">
          Créer une nouvelle liste de cadeaux
        </h1>

        <form onSubmit={handleSubmit}>
          <label htmlFor="nom">Nom de la liste :</label>
          <input
            type="text"
            id="nom"
            name="nom"
            value={nom}
            onChange={(e) => setNom(e.target.value)}
            required
          />

          <label htmlFor="theme">Thème :</label>
          <select
            id="theme"
            name="theme"
            value={theme}
            onChange={(e) => setTheme(e.target.value)}
            required
          >
            <option value="liste_de_mariage">Mariage</option>
            <option value="liste_de_naissance">Naissance</option>
            <option value="liste_anniversaire">Anniversaire</option>
            <option value="liste_de_noel">Noël</option>
            <option value="autres">Autre</option>
          </select>

          <label htmlFor="description">Description :</label>
          <textarea
            id="description"
            name="description"
            value={description}
            onChange={(e) => setDescription(e.target.value)}
            rows="4"
          ></textarea>

          <label htmlFor="dateEvenement">Date de l`évènement :</label>
          <input
            type="date"
            id="dateEvenement"
            name="dateEvenement"
            value={dateEvenement}
            onChange={(e) => setDateEvenement(e.target.value)}
            required
          />

          <button type="submit" className="submit-button">Créer la liste</button>
        </form>
      </div>
      <Footer />
    </>
  );
};

export default CreateWishlistForm;
