import { useState } from "react";
import "../styles/Login.css";
import Header from "../components/Header";
import Footer from "../components/Footer";
import { Link } from "react-router-dom";

const Register = () => {
  const [email, setEmail] = useState("");
  const [password, setPassword] = useState("");

  function handleSubmit(e) {
    e.preventDefault();
    //TODO Ajouter l'envoi des données au serveur/BDD
    console.log("Email:", email);
    console.log("Mot de passe:", password);
    // Réinitialise les champs après la soumission
    setEmail("");
    setPassword("");
  }

  return (
    <>
      <Header />
      <div className="Login-page">
        <h2>Créez votre compte</h2>
        <form onSubmit={handleSubmit}>
          <ul>
            <li>
              <label htmlFor="mail">Votre e-mail</label>
              <input
                type="email"
                id="mail"
                name="user_mail"
                value={email}
                onChange={(e) => setEmail(e.target.value)}
                placeholder="hello@wishmegift.com"
                required
              />
            </li>
            <li>
              <label htmlFor="mail">Votre mot de passe</label>
              <input
                type="password"
                id="password"
                name="user_password"
                value={password}
                onChange={(e) => setPassword(e.target.value)}
                required
              />
            </li>
          </ul>

          <button type="submit" className="login-button">
            Créer mon compte
          </button>
        </form>

        <p>
          Déjà un compte ? <Link to="/login">Connectez-vous</Link>
        </p>
      </div>

      <Footer />
    </>
  );
};

export default Register;
