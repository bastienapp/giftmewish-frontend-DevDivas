import React, { useState } from "react";
import "../styles/Login.css";
import Header from "../components/Header";
import Footer from "../components/Footer";
import { Link } from "react-router-dom";

const Login = () => {
  const [email, setEmail] = useState("");
  const [password, setPassword] = useState("");

  function handleSubmit(e) {
    e.preventDefault();
    //TODO Ajouter l'envoi des données au serveur/BDD
    //TODO ajouter un message d'erreur si champs vides (cf brief)
    console.log("Email:", email);
    console.log("Mot de passe:", password);
    // Réinitialise les champs après la soumission
    setEmail("");
    setPassword("");
  }

  return (
    <>
      <Header />
      <div className="Login-page">
        <h2>Connectez-vous</h2>

        <form onSubmit={handleSubmit}>
          <ul>
            <li>
              <label htmlFor="mail">Votre e-mail</label>
              <input
                type="email"
                id="mail"
                name="user_mail"
                value={email}
                onChange={(e) => setEmail(e.target.value)}
                placeholder="hello@wishmegift.com"
                required
              />
            </li>
            <li>
              <label htmlFor="mail">Votre mot de passe</label>
              <input
                type="password"
                id="password"
                name="user_password"
                value={password}
                onChange={(e) => setPassword(e.target.value)}
                required
              />
            </li>
          </ul>

          <button type="submit" className="login-button">
            Se connecter
          </button>
        </form>

        <p>
          Pas encore de compte ? <Link to="/register"> Inscrivez-vous</Link>
        </p>
      </div>

      <Footer />
    </>
  );
};

export default Login;
